/**
 * 
 */
package servicoMarvel;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.estudo.services.CharacterDataWrapper;
import org.estudo.services.MarvelApplication;
import org.estudo.services.MarvelResponse;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.boot.SpringApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author juca
 *
 */
public class ServiceTest {

	private Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	private Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SpringApplication.run(MarvelApplication.class, "");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	
	}

	@Test
	public void test() {
		assertTrue(true);
	}
	
	@Test
	public void testGetCharacters() {
		
		WebTarget webTarget = client.target("http://localhost:8080/get-characters");	

		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);

		assertTrue(characterDataWrapper != null);
		assertTrue(characterDataWrapper.getCode() == 200);
		assertTrue(characterDataWrapper.getData() != null);

	}

	@Test
	public void testGetCharacter() {
		WebTarget webTarget = client.target("http://localhost:8080/get-character").path("1010870");	

		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);

		assertTrue(characterDataWrapper != null);
		assertTrue(characterDataWrapper.getCode() == 200);
		assertTrue(characterDataWrapper.getData() != null);
		assertTrue(characterDataWrapper.getData().getTotal() == 1);
	}
	
	@Test
	public void testAddCharacter() {
		WebTarget webTarget = client.target("http://localhost:8080/add-character").path("1010870");	

		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(null);
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		assertTrue(json != null);
		assertTrue(json.equals("true"));
	}
	
	@Test
	public void testRemoveCharacter() {
		WebTarget webTarget = client.target("http://localhost:8080/remove-character").path("1010870");	

		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.delete();
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		assertTrue(json != null);
		assertTrue(json.length() >= 4);
	}
	
	@Test
	public void testTotals() {
		WebTarget webTarget = client.target("http://localhost:8080/get-totals");	

		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		assertTrue(json != null);
		assertTrue(json.length() > 2); //the Marvel totals change every week so this verifies generic info
	}

	
	private CharacterDataWrapper getCharacterDataWrapper(WebTarget webTarget) {
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		CharacterDataWrapper characterDataWrapper = gson.fromJson(json, CharacterDataWrapper.class);
		
		return characterDataWrapper;
	}

}
