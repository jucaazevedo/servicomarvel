package org.estudo.services;

import com.google.gson.annotations.Expose;

public class Character {
	@Expose
	private int id; //The unique ID of the character resource.,
	
	@Expose
	private String name; //The name of the character.,
	
	@Expose
	private String description; //A short bio or description of the character.,

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
