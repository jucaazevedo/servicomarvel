package org.estudo.services;

import java.util.List;

import com.google.gson.annotations.Expose;

public class CharacterDataContainer {
	@Expose
	private int offset; //The requested offset (number of skipped results) of the call.,
	
	@Expose
	private int limit;  //The requested result limit.,
	
	@Expose
	private int total; //The total number of resources available given the current filter set.,
	
	@Expose
	private int count; //The total number of results returned by this call.,
	
	@Expose
	private List<Character> results; //The list of characters returned by the call.

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Character> getResults() {
		return results;
	}

	public void setResults(List<Character> results) {
		this.results = results;
	}
}
