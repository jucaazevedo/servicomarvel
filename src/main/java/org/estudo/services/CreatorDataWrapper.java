/**
 * 
 */
package org.estudo.services;

import com.google.gson.annotations.Expose;

/**
 * @author juca
 *
 */
public class CreatorDataWrapper {
	@Expose
	private int code; //The HTTP status code of the returned result.,
	
	@Expose
	private String status; //A string description of the call status.,
	
	@Expose
	private CreatorDataContainer data; //The results returned by the call.,
	
	@Expose
	private String etag; //A digest value of the content returned by the call.
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public CreatorDataContainer getData() {
		return data;
	}
	public void setData(CreatorDataContainer data) {
		this.data = data;
	}
	public String getEtag() {
		return etag;
	}
	public void setEtag(String etag) {
		this.etag = etag;
	}
	
	
}
