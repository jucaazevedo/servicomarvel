/**
 * 
 */
package org.estudo.services;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author juca
 *
 */
@XmlRootElement
public class MarvelResponse {
	
	@XmlElement
	private Integer code;
	
	@XmlElement
	private Integer status;
	

	public MarvelResponse(Integer status) {
		super();
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
