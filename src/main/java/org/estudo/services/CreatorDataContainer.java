/**
 * 
 */
package org.estudo.services;

import com.google.gson.annotations.Expose;

/**
 * @author juca
 *
 */
public class CreatorDataContainer {
	@Expose
	private int total; //The total number of resources available given the current filter set.,

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	
}
