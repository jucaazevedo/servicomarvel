package org.estudo.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class MarvelServiceController {
	
	private static final String apiKey = "3735ef29805d945c1f7f1c1800839aec";
	private static final String privKey = "8f5d986324e9ad5c658e739125ec228c889f1a64";
	private static final String ts = "12345";
	private String hash = MD5(ts + privKey + apiKey);

	private Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	private Set<CharacterDataWrapper> charactersSet = new HashSet<CharacterDataWrapper>();
	private Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
	
	/*
	 * Services
	 */
	
	/**
	 *  THIS METHOD MAKES TWO MARVEL API CALLS
	 *  
	 *   This method returns the number of characters and creators (separated by comma)
	 */
	@RequestMapping(value = "/get-totals", method = RequestMethod.GET)	
    public String getTotals() {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);
		
		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);

		int characterTotal = characterDataWrapper.getData().getTotal();

		webTarget = client.target("http://gateway.marvel.com/v1/public/creators")
				.queryParam("ts", ts)
				.queryParam("apikey", apiKey)
				.queryParam("hash", hash);

		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		CreatorDataWrapper creatorDataWrapper = gson.fromJson(json, CreatorDataWrapper.class);
	
		int creatorsTotal = creatorDataWrapper.getData().getTotal();
		
		String totals = characterTotal + "," + creatorsTotal;
		
		return totals;
	}
	
	/**
	 * Gets all characters
	 * 
	 * @return
	 */
	@RequestMapping(value = "/get-characters", method = RequestMethod.GET)	
    public CharacterDataWrapper getCharacters() {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);
		
		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);

		return characterDataWrapper;
	}

	/**
	 * Gets a character by id
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/get-character/{id}", method = RequestMethod.GET)	
    public CharacterDataWrapper getCharacter(@PathVariable String id) {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.path(id)
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);	

		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);
		return characterDataWrapper;
	}

	/**
	 * Adds a character to a set in memory
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/add-character/{id}", method = RequestMethod.POST)	
    public boolean addCharacter(@PathVariable String id) {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.path(id)
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);	
		 
		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);
		
		boolean added = charactersSet.add(characterDataWrapper);

		return added;
	}

	/**
	 * Removes a character from the set in memory
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/remove-character/{id}", method = RequestMethod.DELETE)	
    public boolean removeCharacter(@PathVariable String id) {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.path(id)
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);	
		 
		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);
		boolean removed = charactersSet.remove(characterDataWrapper);

		return removed;
	}

	/**
	 * Updates the status of a character (only in the set in memory)
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/update-character-status/{id}/{status}", method = RequestMethod.PUT)	
    public boolean updateCharacter(@PathVariable String id, @PathVariable String status) {
		WebTarget webTarget = client.target("http://gateway.marvel.com/v1/public/characters")
									.path(id)
									.queryParam("ts", ts)
									.queryParam("apikey", apiKey)
									.queryParam("hash", hash);	
		 
		CharacterDataWrapper characterDataWrapper = getCharacterDataWrapper(webTarget);
		characterDataWrapper.setStatus(status);
		boolean updated = charactersSet.add(characterDataWrapper);

		return updated;
	}
	
	private CharacterDataWrapper getCharacterDataWrapper(WebTarget webTarget) {
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		response.bufferEntity();
		 
		String json = response.readEntity(String.class);
		
		CharacterDataWrapper characterDataWrapper = gson.fromJson(json, CharacterDataWrapper.class);
		
		return characterDataWrapper;
	}

	/**
	 * generates MD5 hash
	 * @param md5
	 * @return
	 */
	private String MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
		}
		return null;
	}
}